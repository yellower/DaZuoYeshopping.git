import java.util.ArrayList;

public class User {

	private String name;
	private String account;
	private String password;
	CartInformation cart;
	ShoppingCart shoppingcart;
	
	public CartInformation getCart() {
		return cart;
	}
	public void setCart(CartInformation cart) {
		this.cart = cart;
	}
	public ShoppingCart getShoppingcart() {
		return shoppingcart;
	}
	public void setShoppingcart(ShoppingCart shoppingcart) {
		this.shoppingcart = shoppingcart;
	}
	
	ArrayList<CartInformation> cartitem = new ArrayList();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
