package SP;

public class InAndOut 
{
    public abstract String next();
    public abstract void println(String text);
    public abstract int nextInt();
}
