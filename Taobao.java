package SP;

import java.util.Scanner;

public class Menu implements IntAndOut {
	
	public int ShowMenu(){
		println("------------欢迎进入商城！------------");
		println("请选择想进行的操作：");
		println("1.显示购物车内容");
		println("2.添加商品");
		println("3.删除商品");
		println("4.退出系统");
		println("--------------------------------------");

		int n = nextInt();
		return n;
	}

	public void Register() {		//用户注册

		println("请输入账号：");
		int flag = 0;
		while (flag == 0) {
			String s = next();
			if(UserList.userList.size()==0){
				this.user.setAccount(s);
				break;
			}
			for (int i = 0; i < UserList.userList.size(); i++) {
				if (UserList.userList.get(i).getAccount().equals(s)) {
					println("该用户已存在，请重新输入账号：");
					flag = 0;
					// s=next();

					break;
				}
				user.setAccount(s);
				flag = 1;

			}
		}
		
		println("请输入密码：");
		String password = next();
		user.setPassword(password);
		println("请输入用户名：");
		String name = next();
		user.setName(name);
		println("注册成功");
		UserList.userList.add(user);

	}
	public void Login(){
		println("请输入账号：");
		String s = next();
		int flag = 0;
		while(flag==0){
			for (int i = 0; i < UserList.userList.size(); i++) {
				if (UserList.userList.get(i).getAccount().equals(s)) {
					flag=1;
					this.user=UserList.userList.get(i);
					break;
				}
			}
			if(flag==0){
				println("该用户名不存在，请重新输入：");
				s = next();
			}
			
			
		}
		println("请输入密码：");
		String password = next();
		while(flag==1){
			if(!user.getPassword().equals(password)){
				println("密码错误，请重新输入");
				password = next();
			}
			else
				break;
		}
		println("用户"+user.getName()+"登录成功");
		
	}

	@Override
	public String next() {
		String str = sc.next();
		return str;
	}

	@Override
	public void println(String text) {
		System.out.println(text);

	}

	Scanner sc = new Scanner(System.in);
	User user =new User();
	@Override
	public int nextInt() {
		int n = sc.nextInt();
		return n;
	}
	

}